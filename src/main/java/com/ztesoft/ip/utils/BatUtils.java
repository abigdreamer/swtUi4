package com.ztesoft.ip.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.slf4j.Logger;

import com.xiaoleilu.hutool.CharsetUtil;
import com.xiaoleilu.hutool.FileUtil;
import com.xiaoleilu.hutool.Log;

public class BatUtils {
	private static final Logger log = Log.get("batutils");

	/**
	 * @param args
	 */
	public static void runHandSet(String namestr, String ipstr, String ymstr, String wgstr, String dns1str, String dns2str) {
		// TODO Auto-generated method stub
		String c1 = " netsh interface ip set address name=\"" + namestr + "\" static " + ipstr + " " + ymstr + " " + wgstr + " >nul ";
		String c2 = " netsh interface ip add dns \"" + namestr + "\" " + dns1str + " index=1 >nul ";
		String c3 = " netsh interface ip add dns \"" + namestr + "\" " + dns2str + " index=2 >nul ";
		try {
			File f1 = new File("resouce/config/updateIp.bat");
			if (f1.exists()) {
				f1.delete();
			}
			f1.createNewFile();

			FileOutputStream fos = new FileOutputStream(f1);
			String str = System.getProperty("line.separator");
			str = c1 + str + c2 + str + c3 + str;
			OutputStreamWriter osw = new OutputStreamWriter(fos, "GB2312");
			osw.write(str);
			osw.flush();
			osw.close();
			String cmd1 = f1.getAbsolutePath();// pass runbat(cmd1);
			System.out.println(cmd1);
			runbat(cmd1);
			PropertiesUtil.setProperty("ip", ipstr);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void runbat(String batName) {
		try {
			Process p = Runtime.getRuntime().exec(batName);
			System.out.println("bat命令执行完成");

			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "gb2312"));
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println("StartedLog==>" + line);
			}
			br.close();

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public static void runbattest(String batName) {
		runbat("cmd /c java -version");
	}

	public static void runAuto(String namestr) {
		// TODO Auto-generated method stub
		String c1 = " netsh interface ip set address name=\"" + namestr + "\"  source=dhcp >nul ";
		String c2 = " netsh interface ip set dns \"" + namestr + "\"   source=dhcp >nul ";
		try {
			File f1 = new File("resouce/config/updateIp.bat");
			if (f1.exists()) {
				f1.delete();
			}
			f1.createNewFile();

			FileOutputStream fos = new FileOutputStream(f1);
			String str = System.getProperty("line.separator");
			str = c1 + str + c2 + str;
			OutputStreamWriter osw = new OutputStreamWriter(fos, "GB2312");
			osw.write(str);
			osw.flush();
			osw.close();
			String cmd1 = f1.getAbsolutePath();// pass runbat(cmd1);
			System.out.println(cmd1);
			runbat(cmd1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void backOracle(String db, String username, String password, String path) {
		// TODO Auto-generated method stub

		String c1 = "exp " + username + "/" + password + "@" + db + " owner=" + username + " file=" + path + "";
		log.info("开始执行数据库备份脚本:" + c1);
		try {
			String batpath = "resouce/config/exp.bat";
			File bat = null;
			if (!FileUtil.isExist(batpath)) {
				bat = FileUtil.file(batpath);
			} else {
				bat = new File(batpath);
			}
			FileUtil.writeString(c1, batpath, CharsetUtil.ISO_8859_1);
			runbatfile(bat.getAbsolutePath());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("备份执行异常", e);
		}
	}

	public static void main(String[] args) {
		// cmd.exe /k start C:\\automake\\resouce\\config\\updateIp.bat
		try {
			log.debug("a");
			runbatfile("C:\\automake\\resouce\\config\\updateIp.bat");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void runbatfile(String path) throws IOException {
		String k = " cmd.exe /c start " + path + " ";
		log.info("bat脚本文件位置:" + k);
		Process p = Runtime.getRuntime().exec(k);// 要执行的文件的路径为run.bat

		try {
			p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int i = p.exitValue();
		if (i == 0) {
			System.out.println("执行完成.");
		} else {
			System.out.println("执行失败.");
		}
		p.destroy();
		p = null;
		killCmds();
	}

	public static void killCmds() {
		Runtime rt = Runtime.getRuntime();
		Process p = null;
		try {
			rt.exec("cmd.exe /C start wmic process where name='cmd.exe' call terminate");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
