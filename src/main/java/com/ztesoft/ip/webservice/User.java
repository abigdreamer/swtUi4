package com.ztesoft.ip.webservice;

import java.io.File;

import org.slf4j.Logger;

import ci.web.annotaction.Param;
import ci.web.core.CiContext;

import com.xiaoleilu.hutool.Log;

public class User {
	private static final Logger log = Log.get(User.class.getSimpleName());

	// http://127.0.0.1/user/hello

	public void hello(CiContext ctx) {

		System.out.println("User.hello#" + ctx.params().toJSONString());

		ctx.send("User.hello .back from servier!");

	}

	// http://127.0.0.1/user/hello
	// http://127.0.0.1/user/login?email=test@xx.com&pwd=xxx

	public void login(CiContext ctx, @Param("email") String email, @Param("pwd") String passWord) {

		System.out.println("User.login#" + email + " : " + passWord);

		ctx.send("User.login .back from service@");

	}

	public void downfile(CiContext ctx) {
		File as = new File("resouce/config/druid.setting");
		ctx.send(as);

	}
 
}