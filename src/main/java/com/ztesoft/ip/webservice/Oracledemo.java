package com.ztesoft.ip.webservice;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import ci.web.core.CiContext;

import com.xiaoleilu.hutool.db.DbUtil;
import com.xiaoleilu.hutool.db.Entity;
import com.xiaoleilu.hutool.db.handler.EntityListHandler;
import com.xiaoleilu.hutool.db.sql.SqlExecutor;
import com.ztesoft.ip.utils.DruidDatasource;

public class Oracledemo {

	// http://127.0.0.1:8066/oracledemo/shops

	public void shops(CiContext ctx) {
		System.out.println("shops");

		Connection conn = null;
		Integer num = null;
		try {
			conn = DruidDatasource.getConnection();
			List<Entity> entityList = SqlExecutor.query(conn, "SELECT *  FROM user_tables T", new EntityListHandler());
			System.out.println(entityList.size() + "+--------------");
			num = entityList.size();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(conn);
		}

		ctx.send("shops-----------2  " + num);

	}

	public static void main(String[] args) {
		System.out.println("shops");

		Connection conn = null;
		Integer num = null;
		try {
			conn = DruidDatasource.getConnection();
			List<Entity> entityList = SqlExecutor.query(conn, "SELECT *  FROM user_tables T", new EntityListHandler());
			System.out.println(entityList.size() + "+--------------");
			num = entityList.size();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(conn);
		}

	}
}