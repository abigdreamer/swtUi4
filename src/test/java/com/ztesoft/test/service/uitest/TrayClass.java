package com.ztesoft.test.service.uitest;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.ShellAdapter;

import org.eclipse.swt.events.ShellEvent;

import org.eclipse.swt.events.SelectionAdapter;

import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.graphics.Image;

import org.eclipse.swt.widgets.Display;

import org.eclipse.swt.widgets.Shell;

import org.eclipse.swt.widgets.Tray;

import org.eclipse.swt.widgets.TrayItem;

public class TrayClass {

	private static Tray sysTray;

	private static Image trayIcon;

	private static TrayItem tray;

	public static void main(String[] args) {

		final Display display = new Display();

		sysTray = display.getSystemTray();

		trayIcon = new Image(display, "resouce/pictures/png-0004.png");

		final Shell window = new Shell(display);

		window.setImage(trayIcon);

		window.setText("Tray demo");

		window.addShellListener(new ShellAdapter() {

			public void shellIconified(ShellEvent e) {

				if (sysTray != null && tray == null) {// 若同时有几个 ShellEvent,
														// 保证只有一个 tray

					tray = new TrayItem(sysTray, SWT.NONE);

					tray.setImage(trayIcon);

					tray.setToolTipText("Tray demo");

					tray.addSelectionListener(new SelectionAdapter() {

						public void widgetSelected(SelectionEvent e) {

							window.setVisible(true);

							window.setMaximized(true);

							if (tray != null) {

								tray.dispose();

								tray = null;

							}

						}

					});

					window.setMinimized(true);

					window.setVisible(false);

				}

			}

		});

		window.setMaximized(true);

		window.open();

		while (!window.isDisposed()) {

			if (!display.readAndDispatch()) {

				display.sleep();

			}

		}

		display.dispose();

	}

}