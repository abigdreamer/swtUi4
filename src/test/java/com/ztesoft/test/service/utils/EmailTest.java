package com.ztesoft.test.service.utils;

import java.io.File;
import java.io.IOException;

import jodd.io.FileUtil;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import jodd.mail.att.ByteArrayAttachment;
import jodd.mail.att.FileAttachment;

public class EmailTest {
	public static void main(String[] args) throws IOException {
		// 邮件内容图片标签
		EmailAttachment embeddedAttachment = new ByteArrayAttachment(FileUtil.readBytes("pics/makd.png"), "image/png", "c.png", "c.png");
		// 邮件附近
		EmailAttachment attachment = new FileAttachment(new File("pics/png2.png"), "b.jpg", "image/jpeg");

		String title = "收到了";
		String content = "新版本发布";

		Email email = Email.create().from("2642000280@qq.com").to("310336951@qq.com").subject(title).addHtml("<html><META http-equiv=Content-Type content=\"text/html; charset=utf-8\">" + "<body><h1>" + title + "</h1><img src='cid:c.png'>" + content + "</body></html>").embed(embeddedAttachment).attach(attachment).attach(attachment);
		SmtpServer smtpServer = SmtpServer.create("smtp.qq.com").authenticateWith("2642000280@qq.com", "dw222222");
		SendMailSession session = smtpServer.createSession();
		session.open();
		session.sendMail(email);
		session.close();
	}
}
