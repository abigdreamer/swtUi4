package com.ztesoft.test.service.ciweb;

import java.io.File;
import java.io.IOException;

import javax.sql.DataSource;

import com.alibaba.druid.stat.DruidStatService;
import com.alibaba.druid.util.Utils;
import com.xiaoleilu.hutool.Setting;
import com.xiaoleilu.hutool.db.ds.DruidDS;

import ci.web.annotaction.PathParam;
import ci.web.annotaction.Router;
import ci.web.core.CiContext;
import ci.web.router.CiFile;

@Router("druid")
public class DruidStatold {

	private DruidStatService statService = DruidStatService.getInstance();
	File f = new File("resouce/support/http/resources");
	private String resoucePath = f.getAbsolutePath();

	public DruidStatold() {
	}

	@Router("*")
	public void api(CiContext ctx) throws IOException {
		System.out.println("asdf");
		loadFile(ctx);
		return;
		/*if (ctx.path().endsWith(".json") == false) {
			loadFile(ctx);
			return;
		}
		String url = ctx.uri();
		System.out.println("********" + url);
		int idx = url.lastIndexOf('/');
		url = url.substring(idx);
		// System.out.println(url);
		String ret = statService.service(url);
		ctx.send(ret);*/
	}

	private void loadFile(CiContext ctx) throws IOException {
		String filePath = resoucePath + ctx.path().replace("/druid", "");
		System.out.println(filePath);
		if (filePath.endsWith(".jpg")) {
			byte[] bytes = Utils.readByteArrayFromResource(filePath);
			if (bytes != null) {
				ctx.send(bytes);
			}
			return;
		}
		String text = Utils.readFromResource(filePath);
		if (text == null) {
			System.out.println(filePath);
			ctx.redirect("/index.html");
			return;
		}
		if (filePath.endsWith(".css")) {
			ctx.setContentType("text/css;charset=utf-8");
		} else if (filePath.endsWith(".js")) {
			ctx.setContentType("text/javascript;charset=utf-8");
		}
		ctx.send(text);
	}

}
