操作说明


1.corn4j表达式例子
------------------------------------------------------------------------------------
5 * * * *：表示在每时的第5分钟的时候执行一次（如00:05, 01:05, 02:05 etc.）。
* * * * *：表示每分钟执行一次。
* 12 * * Mon：表示每星期一12时的每一分钟执行。

#每天早上6点10分
10 6 * * *
#每两个小时
0 */2 * * *     
#晚上11点到早上8点之间每两个小时，早上8点
0 23-7/2，8 * * *     
#每个月的4号和每个礼拜一到礼拜三的早上11点
0 11  4 * 1-3  
#1月1日早上4点
0 4 1 1 *
-------------------------------------------------------------------------------------
2.布局框架layoututil使用方法
	参考uidemo
	首先shellcomp = LayoutUtils.centerDWdefult(shell, "demo", true, true);shellcomp为全局变量，必须在shell创建之后
	接着shell创建时候一定要给参数0 shell = new Shell(0);
	通过可视化工具创建模型,会自动放在shell里，给shell里拖个控件Composite composite = new Composite(shellcomp, SWT.NONE);把shell换成shellcomp全局变量
	
3.微服务框架
https://github.com/aol/micro-server/wiki/Getting-started-:-Tutorial
https://github.com/aol/micro-server/blob/master/micro-tutorial/src/main/java/app1/simple/MyRestEndPoint.java

4.微服务框架取消--micro-server
	使用http://git.oschina.net/qthis/CiWeb替换。
	web接口提供框架。基于netty封装的简易web服务，适合java-web-api接口开发
	
5.统一的数据库连接池管理  数据库持久层
druid数据库连接池  和数据库持久化的操作，统一从这里取连接DruidDatasource
