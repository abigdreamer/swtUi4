# swtUI4
基于swt，jface等组件封装为一体化的java桌面应用程序框架</br>
从而生成跨平台的可运行，稳定的程序。</br>
 

<h2>服务器自动化运维工具</h2></br>

<h3>发布升级日志</h3></br>
2015年10月29日 21:30:39</br>
1.添加java微服务框架，从而提供web服务接口</br>
2.整理maven工程，添加自动打包可执行jar插件，target目录自动生成压缩包，解压之后即可用</br>
3.工程任务添加，基于特定意义</br>
</br>

2015年10月14日 17:45:07升级V3.0</BR>
1.项目转变模型，从框架平台到实际应用背景方向持续集成；</br>
2.添加数据库自动同步，并已在实际服务器运行，性能稳定效果达到预期;</br>
3.优化和晚上数据库自动备份功能，支持动态添加数据库实例,已在实际服务器运行，性能文档达到预期;</br>

</br>
2015年10月9日 02:16:06升级v2.0</br>
1.添加自动化运维功能数据库自动备份</br>
2.重构项目，支持Linux平台。</br>

v1.0 2015年6月18日 13:50:38</br>
1.初始化项目 </br>
2.业务背景：切换到不同的网络环境设置繁琐的ip dns很麻烦，用跨平台swt来做，界面可以美化，支持用户自定义，目前只支持颜色</br>

v2.0 2015年6月23日 17:42:10</br>
1.改造为maven工程</br>
2.实现更新机制</br>
3.自动修改ip dns  自动获取功能已经正常使用。</br>

v2.1 2015年7月1日 09:02:43</br>
1.发布打包的可执行程序，下载地址</br>
http://pan.baidu.com/s/1c0nT1RU</br>
2.基于swtui4开发的实际项目下载发布，截图预览</br>
http://pan.baidu.com/s/1qW4zXco</br>

  
<h3>界面图片效果预览区域</h3></br>
![image](http://static.oschina.net/uploads/space/2015/1014/175738_rb8e_1458314.png)</br>
![image](http://static.oschina.net/uploads/space/2015/1014/175745_VqcD_1458314.png)</br>
![image](http://static.oschina.net/uploads/space/2015/1014/175755_hgDm_1458314.png)</br>
![image](http://static.oschina.net/uploads/space/2015/1009/021721_JNGK_1458314.png)</br>
![image](http://static.oschina.net/uploads/space/2015/1009/021721_JNGK_1458314.png)</br>
![image](http://static.oschina.net/uploads/space/2015/0626/174539_8cGZ_1458314.png)</br>
![image](http://static.oschina.net/uploads/space/2015/0626/174547_AcJj_1458314.png)</br>
![image](http://static.oschina.net/uploads/space/2015/0626/174554_jdHJ_1458314.png)</br>
![image](http://static.oschina.net/uploads/space/2015/0701/091429_kFzo_1458314.png)</br>
![image](http://static.oschina.net/uploads/space/2015/0701/091809_4rw8_1458314.png)</br>
 
